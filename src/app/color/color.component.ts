import { Component, OnInit } from '@angular/core';
import { PremierService } from '../premier.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.css'],
  providers: [PremierService]
})
export class ColorComponent implements OnInit {

  color = 'red';

  constructor(private premierSer: PremierService, private router: Router ) {

   }

  ngOnInit() {
  }

  changeColor(input){
    this.color = input.value;
  }

  processReq(message: any)
  {
    alert(message);
  }

  loggerMesData() {
    this.premierSer.logger('test');
  }

  goToProfil(){
    const link = ['profil'];
    this.router.navigate(link);
  }

}
