import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilComponent } from './profildevelopper/profil/profil.component';
import { ColorComponent } from './color/color.component';
import { ListeProfilComponent } from './profildevelopper/liste-profil/liste-profil.component';
import { DetailProfilComponent } from './profildevelopper/detail-profil/detail-profil.component';
import { StyleComponent } from './directive/style/style.component';
import { ClassComponent } from './directive/class/class.component';


const routes: Routes = [
  { path: 'profil', component: ProfilComponent },
  { path: 'color', component: ColorComponent },
  { path: 'list', component: ListeProfilComponent },
  { path: 'style', component: StyleComponent },
  { path: 'class', component: ClassComponent },
  { path: '', redirectTo: '/profil', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
