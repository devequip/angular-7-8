import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-router-similation',
  templateUrl: './router-similation.component.html',
  styleUrls: ['./router-similation.component.css']
})
export class RouterSimilationComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToComonent(route) {
    const link = [route];
    this.router.navigate(link);

  }

}
