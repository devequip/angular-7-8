
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
// import { EventEmitter } from 'events';

@Component({
  selector: 'app-fils',
  templateUrl: './fils.component.html',
  styleUrls: ['./fils.component.css']
})
export class FilsComponent implements OnInit {

  @Input() filsProprety;

  @Output() sendRequestToPere = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log('le param filsProperty de fil component =', this.filsProprety);
  }

  sendEvent(){
    this.sendRequestToPere.emit(
        ` Please can i have somme money :)`
    );
  }

}
