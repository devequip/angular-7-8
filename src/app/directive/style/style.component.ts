import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-style',
  templateUrl: './style.component.html',
  styleUrls: ['./style.component.css']
})
export class StyleComponent implements OnInit {

  @Input() color = 'white';
  @Input() bgColor = 'magenta';
  size = '12px';

  constructor() { }

  ngOnInit() {
  }

  change(size){
    this.size = size + 'px';
  }

}
