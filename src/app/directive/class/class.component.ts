import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit {

  teams = [
    'est',
    'barca',
    'milan',
    'psg'
  ]

  est = false;
  barca = true;
  milan = false;

  constructor() { }

  ngOnInit() {
  }
  changeTeam(){
    this.barca = false;
    this.milan = false;
    this.est = true;
  }

}
