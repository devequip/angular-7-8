import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { ChildAComponent } from './child-a/child-a.component';
// import { ChildBComponent } from './child-b/child-b.component';
// import { GrandChildComponent } from './grand-child/grand-child.component';
import { ColorComponent } from './color/color.component';
import { FilsComponent } from './fils/fils.component';
import { FormsModule } from '@angular/forms';

import { ProfilComponent } from './profildevelopper/profil/profil.component';
import { ListeProfilComponent } from './profildevelopper/liste-profil/liste-profil.component';
import { ItemProfilComponent } from './profildevelopper/item-profil/item-profil.component';
import { DetailProfilComponent } from './profildevelopper/detail-profil/detail-profil.component';
import { StyleComponent } from './directive/style/style.component';
import { ClassComponent } from './directive/class/class.component';
import { HighlightDirective } from './directive/highlight.directive';
import { RainbowDirective } from './directive/rainbow.directive';
import { DefaultImagePipe } from './profildevelopper/default-image.pipe';
import { EmbaucheComponent } from './profildevelopper/embauche/embauche.component';
import { HeaderComponent } from './header/header.component';
import { RouterSimilationComponent } from './router-similation/router-similation.component';


@NgModule({
  declarations: [
    AppComponent,
    // ChildAComponent,
    // ChildBComponent,
    // GrandChildComponent,
    ColorComponent,
    FilsComponent,
    ProfilComponent,
    ListeProfilComponent,
    ItemProfilComponent,
    DetailProfilComponent,
    StyleComponent,
    ClassComponent,
    HighlightDirective,
    RainbowDirective,
    DefaultImagePipe,
    EmbaucheComponent,
    HeaderComponent,
    RouterSimilationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
