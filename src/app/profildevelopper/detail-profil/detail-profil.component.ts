import { Component, OnInit, Input } from '@angular/core';
import { PersonneService } from '../Model/Personne.service';
import { EmbaucheService } from '../embauche.service';

@Component({
  selector: 'app-detail-profil',
  templateUrl: './detail-profil.component.html',
  styleUrls: ['./detail-profil.component.css']
})
export class DetailProfilComponent implements OnInit {

  @Input() personne: PersonneService;

  constructor(private embaucheSer: EmbaucheService) { }

  ngOnInit() {
  }

  embaucher(){
    this.embaucheSer.embaucher(this.personne);
  }

}
