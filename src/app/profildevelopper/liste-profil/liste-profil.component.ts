import { PersonneService } from '../Model/Personne.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-liste-profil',
  templateUrl: './liste-profil.component.html',
  styleUrls: ['./liste-profil.component.css']
})
export class ListeProfilComponent implements OnInit {

 @Input() personnes: PersonneService [];
 @Output() selectedPersonne = new EventEmitter();
 

  constructor() { }

  ngOnInit() {
  }

  selectPersonne(selectedPersonne) {
    // console.log(selectedPersonne);
    this.selectedPersonne.emit(
      selectedPersonne
    );
  }

}
