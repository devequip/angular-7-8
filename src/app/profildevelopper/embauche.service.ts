import { Injectable } from '@angular/core';
import { PersonneService } from './Model/Personne.service';

@Injectable({
  providedIn: 'root'
})
export class EmbaucheService {

 private personnes: PersonneService[];

  constructor() {
    this.personnes = [];
  }

  getEmbauchees(): PersonneService[] {
    return this.personnes;
  }

  embaucher(personne: PersonneService): void {
    const index = this.personnes.indexOf(personne);
    if(index < 0){

      this.personnes.push(personne);
    }else{
      alert(`${personne.name} est deja existant !!`);
    }
  }

  debaucher(personne: PersonneService): void {
    const index = this.personnes.indexOf(personne);
    if(index > 0) {
      this.personnes.splice(index, 1);
    }
  }

}
