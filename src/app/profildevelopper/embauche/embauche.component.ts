import { Component, OnInit } from '@angular/core';
import { PersonneService } from '../Model/Personne.service';
import { EmbaucheService } from '../embauche.service';

@Component({
  selector: 'app-embauche',
  templateUrl: './embauche.component.html',
  styleUrls: ['./embauche.component.css']
})
export class EmbaucheComponent implements OnInit {
  personnes: PersonneService[];

  constructor(private embaucheSer: EmbaucheService) {


   }

  ngOnInit() {
    this.personnes = this.embaucheSer.getEmbauchees();
  }

}
