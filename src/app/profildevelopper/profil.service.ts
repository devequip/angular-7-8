import { Injectable } from '@angular/core';
import { PersonneService } from './Model/Personne.service';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  private personnes: PersonneService [];

  constructor() {
    this.personnes = [
      new PersonneService(1, 'Hamza', 'BEDOUI', 40, '4.png', 123456, 'Developper'),
      new PersonneService(2, 'ahmed', 'BEDOUI', 14, '9.png', 123456, 'Articte'),
      new PersonneService(3, 'amine', 'BEDOUI', 9, '7.jpg', 123456, 'footballer'),
      new PersonneService(4, 'Haroun', 'BEDOUI', 7, '2.jpg', 333222, 'eleve'),
      new PersonneService(5, 'Default', 'Test', 33, '', 5677777, 'Test')
    ];

   }

   getPersonne(): PersonneService[]{
     return this.personnes;
   }
}
