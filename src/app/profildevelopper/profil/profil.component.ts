import { Component, OnInit, Input } from '@angular/core';
import { PersonneService } from '../Model/Personne.service';
import { PremierService } from '../../premier.service';
import { ProfilService } from '../profil.service';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  personnes: PersonneService [];
  selectedPersonne: PersonneService;

  constructor(private premierService: PremierService, private profilSer: ProfilService) { }

  ngOnInit() {
    this.personnes = this.profilSer.getPersonne() ;

    this.premierService.logger(this.personnes);
    this.premierService.add('data from profil');

  }

  selectPersonne(personne) {
    this.selectedPersonne = personne;

  }

}
