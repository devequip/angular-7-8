import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PersonneService } from '../Model/Personne.service';


@Component({
  selector: 'app-item-profil',
  templateUrl: './item-profil.component.html',
  styleUrls: ['./item-profil.component.css']
})
export class ItemProfilComponent implements OnInit {

  @Input() personne: PersonneService;
  @Output() selectedPersonne = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  selectPersonne() {
    // TODO emettre un event et y injecter la personne
    this.selectedPersonne.emit(
      this.personne
    );

  }

}
