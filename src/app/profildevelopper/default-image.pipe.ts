import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultImage'
})
export class DefaultImagePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if(!value){
      return '33.png';
    } else {
      return value;
    }
  }

}
